<?php

class Excel_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
  
    public function get_Data()
    {
        #Calling a stored procedure
        $sql = "CALL csv_data";
        $query = $this->db->query($sql);
       
        return $query;
    }
}
