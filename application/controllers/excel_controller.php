<?php

class Excel_controller extends CI_Controller 
{
    function index()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('excelformat', 'Excel Format', 'required|alpha_numeric');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('excel_view');
		}
        else
        {
            $this->generate_excel_file();

        }
    }
    
    public function generate_excel_file()
    {
       
        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'client_description');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'parent_entity_name');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'entity_name');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'trackback_number');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'external_reference');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'effective_start_date_time');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'effective_end_date_time');

        $objPHPExcel->getActiveSheet()->freezePane('A2');
        
        $this->load->dbutil();
        $this->load->model('excel_model');
        $query = $this->excel_model->get_Data();
        
        $objPHPExcel->getActiveSheet()->fromArray($query->result_array(), ' ', 'A2');

     
        if($this->input->post('excelformat') == "xlsx")
        {
        	$filename='data2007.xlsx';
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'"');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
        }
        else
        {
           	$filename='data2003.xls';
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$filename.'"');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output'); 
        }        
           
    }   
    
}

?>