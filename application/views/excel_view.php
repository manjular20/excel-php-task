<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Download Excel File</title>
    </head>
    <body>

    <?php echo validation_errors(); ?>

    <?php echo form_open('excel_controller'); ?>

        <form>

            <h4>
            <label for="dlabel">Download Excel File</label>

            <?php 
                $options = array('xls' => 'Excel 95 (XLS)', 'xlsx' => 'Excel 2007+ (XLSX)');
                echo form_dropdown('excelformat', $options);
            ?> 

            </h4>
            <input type="submit" value="Download" />

        </form>

    </body>
</html>